package io.simplematter.microservices.order.processor;

import io.simplematter.microservices.order.protocol.internal.OrderEvent;
import io.simplematter.microservices.order.protocol.internal.OrderSnapshot;
import io.simplematter.microservices.order.protocol.internal.OrderState;
import io.simplematter.microservices.common.kafka.streams.processor.StoreProcessor;
import io.simplematter.microservices.order.protocol.internal.snapshot.Order;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorSupplier;

public class OrderDispatcher implements ProcessorSupplier<String, OrderEvent> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final String storeName;
    private final String eventDestination;
    private final String snapshotDestination;

    public OrderDispatcher(final String storeName, final String eventDestination, final String snapshotDestination) {

        this.storeName = storeName;
        this.eventDestination = eventDestination;
        this.snapshotDestination = snapshotDestination;
    }

    public Processor<String, OrderEvent> get() {

        return new StoreProcessor<String, OrderEvent, OrderState>() {

            public String getStoreName() {

                return storeName;
            }

            public String getEventDestination() {

                return eventDestination;
            }

            public String getSnapshotDestination() {

                return snapshotDestination;
            }

            @Override
            public void process(final String key, final OrderEvent eventEnvelope) {

                logger.info("DispatcherProcessor received message: " + eventEnvelope.getPayload().getClass().getName());
                //System.out.println(getClass().getSimpleName() + " <- (" + key + ", " + eventEnvelope + ")");
                //System.out.println(getClass().getSimpleName() + " -> (" + key + ", " + eventEnvelope + "->SNAPSHOT)");
                final OrderSnapshot snapshotEnvelope = new OrderSnapshot(eventEnvelope.getId(), eventEnvelope.getCorrelationId());
                snapshotEnvelope.setPayload(Order.create(getState(key), eventEnvelope.getClass().getSimpleName()));
                context().forward(eventEnvelope.getId(), snapshotEnvelope, getSnapshotDestination());
                context().forward(eventEnvelope.getId(), eventEnvelope, getEventDestination());
                context().commit();
            }

        };
    }

}
