package io.simplematter.microservices.order.protocol.internal;

import io.simplematter.microservices.common.protocol.Command;
import io.simplematter.microservices.common.protocol.Envelope;

public class OrderCommand extends Envelope<Command<String>> {

    public OrderCommand(final String id, final String correlationId, final long timeout, final long timestamp) {

        setId(id);
        setCorrelationId(correlationId);
        setTimeout(timeout);
        setTimestamp(timestamp);
    }

    public OrderCommand(final String id, final String correlationId, final long timeout) {

        this(id, correlationId, timeout, System.currentTimeMillis());
    }

    public OrderCommand(final String id, final String correlationId) {

        this(id, correlationId, 10000);
    }

    public OrderCommand() {

    }

}
