package io.simplematter.microservices.order.protocol.converter;

import io.simplematter.microservices.common.protocol.Converter;
import io.simplematter.microservices.order.protocol.avro.OrderEventAvro;
import io.simplematter.microservices.order.protocol.avro.event.OrderConfirmedAvro;
import io.simplematter.microservices.order.protocol.avro.event.OrderRejectedAvro;
import io.simplematter.microservices.order.protocol.avro.event.OrderValidatedAvro;
import io.simplematter.microservices.order.protocol.avro.model.PaymentDetailAvro;
import io.simplematter.microservices.order.protocol.internal.OrderEvent;
import io.simplematter.microservices.order.protocol.internal.event.OrderConfirmed;
import io.simplematter.microservices.order.protocol.internal.event.OrderRejected;
import io.simplematter.microservices.order.protocol.internal.event.OrderValidated;
import io.simplematter.microservices.order.protocol.internal.model.PaymentDetail;

public class EventConverter implements Converter<OrderEvent, OrderEventAvro> {

    @Override
    public OrderEventAvro serialize(final OrderEvent source) {

        final OrderEventAvro sink = new OrderEventAvro();
        sink.setId(source.getId());
        sink.setCorrelationId(source.getCorrelationId());
        sink.setManifest(source.getManifest());
        sink.setTimeout(source.getTimeout());
        sink.setTimestamp(source.getTimestamp());
        if (source.getPayload() instanceof OrderValidated) {
            sink.setOrderValidated(convert((OrderValidated) source.getPayload()));
        } else if (source.getPayload() instanceof OrderConfirmed) {
            sink.setOrderConfirmed(convert((OrderConfirmed) source.getPayload()));
        } else if (source.getPayload() instanceof OrderRejected) {
            sink.setOrderRejected(convert((OrderRejected) source.getPayload()));
        }
        return sink;
    }

    @Override
    public OrderEvent deserialize(final OrderEventAvro source) {

        final OrderEvent sink = new OrderEvent();
        sink.setId(source.getId().toString());
        sink.setCorrelationId(source.getCorrelationId().toString());
        sink.setTimeout(source.getTimeout());
        sink.setTimestamp(source.getTimestamp());
        if (source.getOrderValidated() != null) {
            sink.setPayload(convert(source.getOrderValidated()));
        } else if (source.getOrderConfirmed() != null) {
            sink.setPayload(convert(source.getOrderConfirmed()));
        } else if (source.getOrderRejected() != null) {
            sink.setPayload(convert(source.getOrderRejected()));
        }
        return sink;
    }

    private OrderValidatedAvro convert(final OrderValidated source) {

        final PaymentDetailAvro paymentDetail = new PaymentDetailAvro();
        paymentDetail.setCardHolder(source.getPaymentDetail().getCardHolder());
        paymentDetail.setCardType(source.getPaymentDetail().getCardType());
        paymentDetail.setCardNumber(source.getPaymentDetail().getCardNumber());
        paymentDetail.setAmount(source.getPaymentDetail().getAmount());
        paymentDetail.setCurrency(source.getPaymentDetail().getCurrency());

        final OrderValidatedAvro sink = new OrderValidatedAvro();
        sink.setId(source.getId());
        sink.setSku(source.getSku());
        sink.setQuantity(source.getQuantity());
        sink.setPrice(source.getPrice());
        sink.setPaymentDetail(paymentDetail);
        sink.setCustomerId(source.getCustomerId());
        sink.setCheckoutId(source.getCheckoutId());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private OrderValidated convert(final OrderValidatedAvro source) {

        final PaymentDetail paymentDetail = new PaymentDetail();
        paymentDetail.setCardHolder(source.getPaymentDetail().getCardHolder().toString());
        paymentDetail.setCardType(source.getPaymentDetail().getCardType().toString());
        paymentDetail.setCardNumber(source.getPaymentDetail().getCardNumber().toString());
        paymentDetail.setAmount(source.getPaymentDetail().getAmount());
        paymentDetail.setCurrency(source.getPaymentDetail().getCurrency().toString());

        final OrderValidated sink = new OrderValidated();
        sink.setId(source.getId().toString());
        sink.setSku(source.getSku().toString());
        sink.setQuantity(source.getQuantity());
        sink.setPrice(source.getPrice());
        sink.setPaymentDetail(paymentDetail);
        sink.setCustomerId(source.getCustomerId().toString());
        sink.setCheckoutId(source.getCheckoutId().toString());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private OrderConfirmedAvro convert(final OrderConfirmed source) {

        final OrderConfirmedAvro sink = new OrderConfirmedAvro();
        sink.setId(source.getId());
        sink.setCheckoutId(source.getCheckoutId());
        sink.setPaymentId(source.getPaymentId());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private OrderConfirmed convert(final OrderConfirmedAvro source) {

        final OrderConfirmed sink = new OrderConfirmed();
        sink.setId(source.getId().toString());
        sink.setCheckoutId(source.getCheckoutId().toString());
        sink.setPaymentId(source.getPaymentId().toString());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private OrderRejectedAvro convert(final OrderRejected source) {

        final OrderRejectedAvro sink = new OrderRejectedAvro();
        sink.setId(source.getId());
        sink.setReason(source.getReason());
        sink.setCheckoutId(source.getCheckoutId());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private OrderRejected convert(final OrderRejectedAvro source) {

        final OrderRejected sink = new OrderRejected();
        sink.setId(source.getId().toString());
        sink.setReason(source.getReason().toString());
        sink.setCheckoutId(source.getCheckoutId().toString());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }
}
