package io.simplematter.microservices.order.protocol.converter;

import io.simplematter.microservices.common.protocol.Converter;
import io.simplematter.microservices.order.protocol.avro.OrderCommandAvro;
import io.simplematter.microservices.order.protocol.avro.command.ConfirmOrderAvro;
import io.simplematter.microservices.order.protocol.avro.command.RejectOrderAvro;
import io.simplematter.microservices.order.protocol.avro.command.ValidateOrderAvro;
import io.simplematter.microservices.order.protocol.avro.model.PaymentDetailAvro;
import io.simplematter.microservices.order.protocol.internal.OrderCommand;
import io.simplematter.microservices.order.protocol.internal.command.ConfirmOrder;
import io.simplematter.microservices.order.protocol.internal.command.RejectOrder;
import io.simplematter.microservices.order.protocol.internal.command.ValidateOrder;
import io.simplematter.microservices.order.protocol.internal.model.PaymentDetail;

public class CommandConverter implements Converter<OrderCommand, OrderCommandAvro> {

    @Override
    public OrderCommandAvro serialize(final OrderCommand source) {

        final OrderCommandAvro sink = new OrderCommandAvro();
        sink.setId(source.getId());
        sink.setCorrelationId(source.getCorrelationId());
        sink.setTimeout(source.getTimeout());
        sink.setManifest(source.getManifest());
        sink.setTimestamp(source.getTimestamp());
        if (source.getPayload() instanceof ValidateOrder) {
            sink.setValidateOrder(convert((ValidateOrder) source.getPayload()));
        } else if (source.getPayload() instanceof ConfirmOrder) {
            sink.setConfirmOrder(convert((ConfirmOrder) source.getPayload()));
        } else if (source.getPayload() instanceof RejectOrder) {
            sink.setRejectOrder(convert((RejectOrder) source.getPayload()));
        }
        return sink;
    }

    @Override
    public OrderCommand deserialize(final OrderCommandAvro source) {

        final OrderCommand sink = new OrderCommand();
        sink.setId(source.getId().toString());
        sink.setCorrelationId(source.getCorrelationId().toString());
        sink.setTimeout(source.getTimeout());
        sink.setTimestamp(source.getTimestamp());
        if (source.getValidateOrder() != null) {
            sink.setPayload(convert(source.getValidateOrder()));
        } else if (source.getConfirmOrder() != null) {
            sink.setPayload(convert(source.getConfirmOrder()));
        } else if (source.getRejectOrder() != null) {
            sink.setPayload(convert(source.getRejectOrder()));
        }
        return sink;
    }

    private ValidateOrderAvro convert(final ValidateOrder source) {

        final PaymentDetailAvro paymentDetail = new PaymentDetailAvro();
        paymentDetail.setCardHolder(source.getPaymentDetail().getCardHolder());
        paymentDetail.setCardType(source.getPaymentDetail().getCardType());
        paymentDetail.setCardNumber(source.getPaymentDetail().getCardNumber());
        paymentDetail.setAmount(source.getPaymentDetail().getAmount());
        paymentDetail.setCurrency(source.getPaymentDetail().getCurrency());

        final ValidateOrderAvro sink = new ValidateOrderAvro();
        sink.setId(source.getId());
        sink.setSku(source.getSku());
        sink.setQuantity(source.getQuantity());
        sink.setPrice(source.getPrice());
        sink.setPaymentDetail(paymentDetail);
        sink.setCustomerId(source.getCustomerId());
        sink.setCheckoutId(source.getCheckoutId());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private ValidateOrder convert(final ValidateOrderAvro source) {

        final PaymentDetail paymentDetail = new PaymentDetail();
        paymentDetail.setCardHolder(source.getPaymentDetail().getCardHolder().toString());
        paymentDetail.setCardType(source.getPaymentDetail().getCardType().toString());
        paymentDetail.setCardNumber(source.getPaymentDetail().getCardNumber().toString());
        paymentDetail.setAmount(source.getPaymentDetail().getAmount());
        paymentDetail.setCurrency(source.getPaymentDetail().getCurrency().toString());

        final ValidateOrder sink = new ValidateOrder();
        sink.setId(source.getId().toString());
        sink.setSku(source.getSku().toString());
        sink.setQuantity(source.getQuantity());
        sink.setPrice(source.getPrice());
        sink.setPaymentDetail(paymentDetail);
        sink.setCustomerId(source.getCustomerId().toString());
        sink.setCheckoutId(source.getCheckoutId().toString());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private ConfirmOrderAvro convert(final ConfirmOrder source) {

        final ConfirmOrderAvro sink = new ConfirmOrderAvro();
        sink.setId(source.getId());
        sink.setPaymentId(source.getPaymentId());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private ConfirmOrder convert(final ConfirmOrderAvro source) {

        final ConfirmOrder sink = new ConfirmOrder();
        sink.setId(source.getId().toString());
        sink.setPaymentId(source.getPaymentId().toString());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private RejectOrderAvro convert(final RejectOrder source) {

        final RejectOrderAvro sink = new RejectOrderAvro();
        sink.setId(source.getId());
        sink.setReason(source.getReason());
        sink.setPaymentId(source.getPaymentId());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private RejectOrder convert(final RejectOrderAvro source) {

        final RejectOrder sink = new RejectOrder();
        sink.setId(source.getId().toString());
        sink.setReason(source.getReason().toString());
        sink.setPaymentId(source.getPaymentId() == null ? null : source.getPaymentId().toString());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }
}
