package io.simplematter.microservices.order.protocol.serdes;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.simplematter.microservices.common.protocol.serdes.SerdesSupplier;
import io.simplematter.microservices.common.kafka.serialization.AvroDeserializer;
import io.simplematter.microservices.common.kafka.serialization.AvroSerializer;
import io.simplematter.microservices.order.protocol.avro.OrderEventAvro;
import io.simplematter.microservices.order.protocol.converter.EventConverter;
import io.simplematter.microservices.order.protocol.internal.OrderEvent;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

import java.util.HashMap;

public class EventSerdes extends SerdesSupplier<OrderEvent> {

    public EventSerdes(final String applicationId, final SchemaRegistryClient schemaRegistry, final String registryUrl) {

        super(applicationId, schemaRegistry, registryUrl);
    }

    @Override
    public AvroSerializer<OrderEvent> getSerializer() {

        final AvroSerializer<OrderEvent> serializer = new AvroSerializer(getApplicationId(), getSchemaRegistry(), new EventConverter());

        serializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return serializer;
    }

    @Override
    public AvroDeserializer<OrderEvent> getDeserializer() {

        final AvroDeserializer<OrderEvent> deserializer = new AvroDeserializer(getApplicationId(), getSchemaRegistry(), new EventConverter(), OrderEventAvro.SCHEMA$);

        deserializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return deserializer;
    }

    @Override
    public Serde<OrderEvent> getSerdes() {

        return Serdes.serdeFrom(getSerializer(), getDeserializer());
    }

}
