package io.simplematter.microservices.order.protocol.internal.event;

import io.simplematter.microservices.common.protocol.Event;

public class OrderRejected implements Event<String> {

    private String id;
    private String reason;
    private String checkoutId;
    private long timestamp;

    public void setId(String id) {
        this.id = id;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setCheckoutId(String checkoutId) {
        this.checkoutId = checkoutId;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getReason() {
        return reason;
    }

    public String getCheckoutId() {
        return checkoutId;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }
}
