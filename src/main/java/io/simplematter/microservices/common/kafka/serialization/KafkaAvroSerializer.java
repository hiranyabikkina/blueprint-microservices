package io.simplematter.microservices.common.kafka.serialization;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.confluent.kafka.serializers.*;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class KafkaAvroSerializer extends AbstractKafkaAvroSerializer implements Serializer<Object> {

    private String applicationId;
    private boolean isKey;

    public KafkaAvroSerializer(String applicationId) {
        this.applicationId = applicationId;
    }

    public KafkaAvroSerializer(SchemaRegistryClient client, String applicationId) {
        this.schemaRegistry = client;
        this.applicationId = applicationId;
    }

    public KafkaAvroSerializer(SchemaRegistryClient client, Map<String, ?> props) {
        this.schemaRegistry = client;
        this.configure(this.serializerConfig(props));
    }

    public void configure(Map<String, ?> configs, boolean isKey) {
        this.isKey = isKey;
        this.configure(new KafkaAvroSerializerConfig(configs));
    }

    public byte[] serialize(String topic, Object record) {
        return this.serializeImpl(getSubjectName(applicationId, topic, this.isKey), record);
    }

    public void close() {
    }
}
