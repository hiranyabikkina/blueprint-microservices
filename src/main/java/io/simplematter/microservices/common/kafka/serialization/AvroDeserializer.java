package io.simplematter.microservices.common.kafka.serialization;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.simplematter.microservices.common.protocol.Converter;
import org.apache.avro.Schema;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

public class AvroDeserializer<T> implements Deserializer<T> {

    private String applicationId;
    private final KafkaAvroDeserializer deserializer;
    private final Converter converter;
    private Schema readerSchema;

    public AvroDeserializer(final String applicationId, final SchemaRegistryClient schemaRegistry, final Converter converter) {

        this.applicationId = applicationId;
        this.deserializer = new KafkaAvroDeserializer(schemaRegistry, applicationId);
        this.converter = converter;
    }

    public AvroDeserializer(final String applicationId, final SchemaRegistryClient schemaRegistry, final Converter converter, final Schema readerSchema) {

        this.applicationId = applicationId;
        this.deserializer = new KafkaAvroDeserializer(schemaRegistry, applicationId);
        this.converter = converter;
        this.readerSchema = readerSchema;
    }

    public void configure(final Map<String, ?> configs, final boolean isKey) {

        deserializer.configure(configs, isKey);
    }

    public T deserialize(String topic, final byte[] bytes) {

        if (topic.startsWith("KSTREAM-")) {
            topic = applicationId + "-" + topic;
        }
        //System.out.println("Reading from " + topic);
        final Object value = deserializer.deserialize(topic, bytes, readerSchema);
        return value == null ? null : (T) converter.deserialize(value);
    }

    public void close() {
        deserializer.close();
    }
}
