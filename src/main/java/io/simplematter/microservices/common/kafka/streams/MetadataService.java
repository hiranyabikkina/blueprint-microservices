package io.simplematter.microservices.common.kafka.streams;

import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.state.StreamsMetadata;

import java.util.ArrayList;
import java.util.List;

public class MetadataService<K> {

    private final KafkaStreams stream;

    public MetadataService(final KafkaStreams stream) {

        this.stream = stream;
    }

    public List<HostStoreInfo> streamsMetadata() {

        return map(stream.allMetadata());
    }

    public List<HostStoreInfo> streamsMetadataForStore(final String name) {

        return map(stream.allMetadataForStore(name));
    }

    public HostStoreInfo streamsMetadataForStoreAndKey(final String name, final K key, final Serializer<K> serializer) {

        final StreamsMetadata metadata = stream.metadataForKey(name, key, serializer);
        if (metadata == null) {
            throw new RuntimeException("Cannot find metadata name: " + name + " key: " + key);
        }
        return new HostStoreInfo(metadata.host(),
                metadata.port(),
                metadata.stateStoreNames());
    }

    private List<HostStoreInfo> map(final Iterable<StreamsMetadata> metadatas) {

        final List<HostStoreInfo> infos = new ArrayList<>();
        metadatas.forEach(metadata -> {
            infos.add(new HostStoreInfo(metadata.host(),
                    metadata.port(),
                    metadata.stateStoreNames()));
        });
        return infos;
    }
}
