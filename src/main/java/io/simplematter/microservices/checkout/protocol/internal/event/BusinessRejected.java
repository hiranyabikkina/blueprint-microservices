package io.simplematter.microservices.checkout.protocol.internal.event;

import io.simplematter.microservices.common.protocol.Event;

public class BusinessRejected implements Event<String> {

    private String id;
    private String origId;
    private String reason;
    private long timestamp;

    public BusinessRejected() {

    }

    public void setId(String id) {
        this.id = id;
    }

    public void setOrigId(String origId) {
        this.origId = origId;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getOrigId() {
        return origId;
    }

    public String getReason() {
        return reason;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }
}
