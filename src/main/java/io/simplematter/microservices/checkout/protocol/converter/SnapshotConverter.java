package io.simplematter.microservices.checkout.protocol.converter;

import io.simplematter.microservices.checkout.protocol.avro.CheckoutSnapshotAvro;
import io.simplematter.microservices.checkout.protocol.avro.snapshot.CheckoutAvro;
import io.simplematter.microservices.checkout.protocol.internal.CheckoutSnapshot;
import io.simplematter.microservices.checkout.protocol.internal.snapshot.Checkout;
import io.simplematter.microservices.common.protocol.Converter;

public class SnapshotConverter implements Converter<CheckoutSnapshot, CheckoutSnapshotAvro> {

    @Override
    public CheckoutSnapshotAvro serialize(final CheckoutSnapshot source) {

        final CheckoutSnapshotAvro sink = new CheckoutSnapshotAvro();
        sink.setId(source.getId());
        sink.setCorrelationId(source.getCorrelationId());
        sink.setTimeout(source.getTimeout());
        sink.setManifest(source.getManifest());
        sink.setTimestamp(source.getTimestamp());
        if (source.getPayload() instanceof Checkout) {
            sink.setPayload(serialize((Checkout)
                    source.getPayload()));
        }
        return sink;
    }

    @Override
    public CheckoutSnapshot deserialize(final CheckoutSnapshotAvro source) {

        final CheckoutSnapshot sink = new CheckoutSnapshot();
        sink.setId(source.getId().toString());
        sink.setCorrelationId(source.getCorrelationId().toString());
        sink.setTimeout(source.getTimeout());
        sink.setTimestamp(source.getTimestamp());
        if (source.getPayload() != null) {
            sink.setPayload(deserialize(source.getPayload()));
        }
        return sink;
    }

    private CheckoutAvro serialize(final Checkout source) {

        final CheckoutAvro sink = new CheckoutAvro();
        sink.setId(source.getId());
        sink.setSku(source.getSku());
        sink.setQuantity(source.getQuantity());
        sink.setPrice(source.getPrice());
        sink.setCustomerId(source.getCustomerId());
        sink.setOrderId(source.getOrderId());
        sink.setPaymentId(source.getPaymentId());
        sink.setStatus(source.getStatus());
        sink.setEventId(source.getEventId());
        sink.setEventName(source.getEventName());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private Checkout deserialize(final CheckoutAvro source) {

        final Checkout sink = new Checkout();
        sink.setId(source.getId().toString());
        sink.setSku(source.getSku().toString());
        sink.setQuantity(source.getQuantity());
        sink.setPrice(source.getPrice());
        sink.setCustomerId(source.getCustomerId().toString());
        sink.setOrderId(source.getOrderId() == null ? null : source.getOrderId().toString());
        sink.setPaymentId(source.getPaymentId() == null ? null : source.getPaymentId().toString());
        sink.setStatus(source.getStatus().toString());
        sink.setEventId(source.getEventId().toString());
        sink.setEventName(source.getEventName().toString());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }
}
