package io.simplematter.microservices.checkout.protocol.internal.snapshot;

import io.simplematter.microservices.common.protocol.Snapshot;

public class Checkout implements Snapshot<String, String> {

    private String id;
    private String sku;
    private int quantity;
    private double price;
    private String customerId;
    private String orderId;
    private String paymentId;
    private String status;
    private String eventId;
    private String eventName;
    private long timestamp;

    public static Checkout create(final io.simplematter.microservices.checkout.protocol.internal.state.Checkout source,
                                  final String eventName) {

        final io.simplematter.microservices.checkout.protocol.internal.snapshot.Checkout sink =
                new io.simplematter.microservices.checkout.protocol.internal.snapshot.Checkout();
        sink.setId(source.getId());
        sink.setSku(source.getSku());
        sink.setQuantity(source.getQuantity());
        sink.setPrice(source.getPrice());
        sink.setCustomerId(source.getCustomerId());
        sink.setOrderId(source.getOrderId());
        sink.setPaymentId(source.getPaymentId());
        sink.setStatus(source.getStatus());
        sink.setEventId(source.getEventId());
        sink.setEventName(eventName);
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getSku() {
        return sku;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String getEventId() {
        return eventId;
    }

    public String getEventName() {
        return eventName;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }
}
