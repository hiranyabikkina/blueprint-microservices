package io.simplematter.microservices.checkout.protocol.internal.event;

import io.simplematter.microservices.common.protocol.Event;

public class CheckoutAcknowledged implements Event<String> {

    private String id;
    private String orderId;
    private long timestamp;

    public void setId(String id) {
        this.id = id;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getOrderId() {
        return orderId;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }
}
