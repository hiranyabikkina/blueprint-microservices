package io.simplematter.microservices.checkout.protocol.internal;

import io.simplematter.microservices.common.protocol.Envelope;
import io.simplematter.microservices.common.protocol.State;

public class CheckoutState extends Envelope<State<String>> {

    public CheckoutState(final String id, final String correlationId, final long timeout, final long timestamp) {

        setId(id);
        setCorrelationId(correlationId);
        setTimeout(timeout);
        setTimestamp(timestamp);
    }

    public CheckoutState(final String id, final String correlationId, final long timeout) {

        this(id, correlationId, timeout, System.currentTimeMillis());
    }

    public CheckoutState(final String id, final String correlationId) {

        this(id, correlationId, 10000);
    }

    public CheckoutState() {

    }

}
