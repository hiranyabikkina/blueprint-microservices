package io.simplematter.microservices.checkout;

import io.simplematter.microservices.checkout.vertx.StartupVerticle;
import io.vertx.core.Vertx;

public class CheckoutService {

    public static void main(String args[]) {

        final Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new StartupVerticle());
    }
}
