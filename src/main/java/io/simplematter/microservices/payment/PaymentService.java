package io.simplematter.microservices.payment;

import io.simplematter.microservices.payment.vertx.StartupVerticle;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

public class PaymentService {

    public static void main(String args[]) {

        final Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new StartupVerticle());
    }
}
